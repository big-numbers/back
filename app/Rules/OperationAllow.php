<?php

namespace App\Rules;

use App\Models\Operation;
use Illuminate\Contracts\Validation\Rule;

/**
 * Check operation allow
 */
class OperationAllow implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return Operation::query()
            ->whereId($value)
            ->where('allow', true)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Operation deny.';
    }
}
