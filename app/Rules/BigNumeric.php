<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Rules for BigNumeric
 */
class BigNumeric implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_match('/^[1-9][0-9]+$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Numeric has an invalid format.';
    }
}
