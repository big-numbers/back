<?php

namespace App\Console\Commands\Fill;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Filling users
 */
class Users extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command filling users table';

    /**
     * User list
     *
     * @var array
     */
    private const USER_LIST = [
        [
            'name'      => 'Admin',
            'email'     => 'admin@calc.net',
            'password'  => '123456',
        ],
    ];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start creating users');

        DB::transaction(function () {
            foreach (self::USER_LIST as $item) {
                $user = new User;
                $user->name = $item['name'];
                $user->email = $item['email'];
                $user->password = bcrypt($item['password']);
                $user->saveOrFail();
            }
        });

        $this->info('Finish creating');

        return 0;
    }
}
