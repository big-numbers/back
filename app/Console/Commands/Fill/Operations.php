<?php

namespace App\Console\Commands\Fill;

use App\Models\Operation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

/**
 * Filling operation
 */
class Operations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:operations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command filling operations table';

    /**
     * User list
     *
     * @var array
     */
    private const OPERATION_LIST = [
        [
            'id'    => Operation\Id::ADDITION,
            'name'  => 'addition',
            'allow' => true,
        ],
        [
            'id'    => Operation\Id::SUBTRACTION,
            'name'  => 'subtraction',
            'allow' => true,
        ],
    ];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start creating operation');

        DB::transaction(function () {
            foreach (self::OPERATION_LIST as $item) {
                $user        = new Operation();
                $user->id    = $item['id'];
                $user->name  = $item['name'];
                $user->allow = $item['allow'];
                $user->saveOrFail();
            }
        });

        $this->info('Finish creating');

        return 0;
    }
}
