<?php


namespace App\Models\Operation;


final class Id
{
    const ADDITION = 1;
    const SUBTRACTION = 2;
}
