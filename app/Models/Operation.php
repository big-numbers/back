<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Operation model
 *
 * @property int             $id
 * @property string          $name
 * @property bool            $allow
 * @property Carbon|null     $created_at
 * @property Carbon|null     $updated_at
 */
class Operation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'allow',
    ];
}
