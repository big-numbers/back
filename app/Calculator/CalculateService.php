<?php

namespace App\Calculator;

/**
 * Calculate service
 */
class CalculateService
{
    /**
     * Calculate big numbers
     *
     * @param string[] $numbers
     * @param int      $operation
     *
     * @return string
     * @throws OperationNotFoundException
     */
    public function calculate($numbers, $operation): string
    {
        return (new Calculator(
            $numbers,
            OperationBuilder::create($operation)
        ))->calc();
    }
}
