<?php

namespace App\Calculator\Operations;

/**
 * Subtraction calculate
 */
class Subtraction implements Operation
{
    /**
     * Subtraction two bid number
     *
     * @param string[] $numbers
     * @return string
     */
    public function calc(array $numbers): string
    {
        list($first, $second) = $numbers;

        return bcsub($first, $second);
    }
}
