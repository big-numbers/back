<?php

namespace App\Calculator\Operations;

/**
 * Addition calculate
 */
class Addition implements Operation
{
    /**
     * Addition two bid number
     *
     * @param string[] $numbers
     * @return string
     */
    public function calc(array $numbers): string
    {
        list($first, $second) = $numbers;

        return bcadd($first, $second);
    }
}
