<?php

namespace App\Calculator\Operations;

/**
 * Interface Operation
 */
interface Operation
{
    /**
     * @param string[]
     * @return string
     */
    public function calc(array $numbers): string;
}
