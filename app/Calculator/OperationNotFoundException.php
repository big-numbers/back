<?php

namespace App\Calculator;

use Exception;

/**
 * OperationNotFoundException
 */
class OperationNotFoundException extends Exception
{
}
