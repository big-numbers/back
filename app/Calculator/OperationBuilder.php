<?php

namespace App\Calculator;

use App\Calculator\Operations\Operation;
use App\Models\Operation as OperationModel;

/**
 * Operation builder
 */
class OperationBuilder
{
    /**
     * Create operation
     *
     * @param int $operation
     * @return Operation
     * @throws OperationNotFoundException
     */
    public static function create(int $operation): Operation
    {
        switch ($operation) {
            case OperationModel\Id::ADDITION:
                return new Operations\Addition;
            case OperationModel\Id::SUBTRACTION:
                return new Operations\Subtraction;
        }

        throw new OperationNotFoundException('Not found calculate operation');
    }
}
