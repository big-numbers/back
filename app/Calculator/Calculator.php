<?php

namespace App\Calculator;

use App\Calculator\Operations\Operation;

/**
 * Calculator big number
 */
class Calculator
{
    /**
     * Pair big numbers
     *
     * @var string[]
     */
    private $numbers;

    /**
     * @var Operation
     */
    private $operation;

    /**
     * Create calculator
     *
     * @param string[]  $numbers
     * @param Operation $operation
     */
    public function __construct($numbers, $operation)
    {
        $this->numbers  = $numbers;
        $this->operation = $operation;
    }

    /**
     * Calculate big number
     */
    public function calc()
    {
        return $this->operation->calc($this->numbers);
    }
}
