<?php


namespace App\Http\Controllers\Api;

use App\Calculator\CalculateService;
use App\Calculator\OperationNotFoundException;
use App\Http\Requests\CalculateRequest;
use App\Http\Controllers\Controller;

/**
 * Calculate controller
 */
class CalculateController extends Controller
{
    /**
     * Calculate the value of the expression
     *
     * @param CalculateRequest $request
     * @param CalculateService $service
     *
     * @return array
     * @throws OperationNotFoundException
     */
    function calc(CalculateRequest $request, CalculateService $service): array
    {
        return [
            'result' => $service->calculate(
                $request->numbers,
                $request->operation
            )
        ];
    }
}
