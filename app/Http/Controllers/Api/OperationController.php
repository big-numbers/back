<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Operation;
use Illuminate\Http\JsonResponse;
use Throwable;

/**
 * Calculate controller
 */
class OperationController extends Controller
{
    /**
     * Get operation list
     *
     * @return JsonResponse
     */
    function index(): JsonResponse
    {
        return response()->json(Operation::all());
    }

    /**
     * Enable operation
     *
     * @param int $id
     *
     * @return JsonResponse
     * @throws Throwable
     */
    function enable(int $id): JsonResponse
    {
        $operation = Operation::query()->findOrFail($id);
        $operation->allow = true;
        $operation->saveOrFail();

        return response()->json(null, 200);
    }

    /**
     * Disable operation
     *
     * @param int $id
     *
     * @return JsonResponse
     * @throws Throwable
     */
    function disable(int $id): JsonResponse
    {
        $operation = Operation::query()->findOrFail($id);
        $operation->allow = false;
        $operation->saveOrFail();

        return response()->json(null, 200);
    }
}
