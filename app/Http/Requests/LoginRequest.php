<?php


namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Login request
 *
 * @property int $id
 */
class LoginRequest extends FormRequest
{
    /**
     * Roles for validate login form
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ];
    }
}
