<?php


namespace App\Http\Requests;

use App\Rules\BigNumeric;
use App\Rules\OperationAllow;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Calculate request
 *
 * @property string[] $numbers
 * @property int      $operation
 */
class CalculateRequest extends FormRequest
{
    /**
     * Roles for validate calculate
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numbers'     => 'required|array|size:2',
            'numbers.*'   => [
                'bail',
                'string',
                'required',
                new BigNumeric(),
            ],
            'operation'   => [
                'bail',
                'integer',
                'required',
                'exists:operations,id',
                new OperationAllow(),
            ],
        ];
    }
}
