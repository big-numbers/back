import Vue from "vue";
import Router from "vue-router";
import HomePage from "./pages/home.vue";
import MainLayout from "./layouts/main"

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            component: MainLayout,
            children: [
                {
                    path: "/user/:id",
                    component: HomePage,
                    props: true,
                    children: [
                        {
                            path: "",
                            component: HomePage
                        },
                        {
                            path: "project",
                            component: HomePage
                        }
                    ]
                }
            ]
        },
    ],
    mode: "history"
});
