import Vue from "vue";
import App from "./App.vue";
import vuetify from "./vuetify";
import router from "./router";

Vue.config.productionTip = false;

new Vue({
    vuetify,
    router,
    render: h => h(App)
}).$mount("#app");

//
// import Vuetify from 'vuetify'
// import VueRouter from 'vue-router'
// // import App from './App'
//
// Vue.component('sidebar', {
//     template: '#navbar',
//     props: {
//         items: Array
//     },
// })
//
// const Home = {
//     template: '#home',
//     data: () => ({
//         sparklineData: [
//             423,
//             446,
//             675,
//             510,
//             590,
//             610,
//             423,
//         ],
//     }),
// }
//
// const Page = {
//     template: '#page'
// }
//
// const routes = [
//     { path:'/', component: Home },
//     { path:'/detections', component: Page },
//     { path:'/comp', component: Page },
//     { path:'/customers', component: Page },
//     { path:'/orders', component: Page },
//     { path:'/settings', component: Page },
// ]
//
// const router = new VueRouter({
//     routes
// })
//
// const app = new Vue({
//     el: '#app',
//     router: router,
//     vuetify: new Vuetify(),
//     // render: h => h(App)
// })
//
// // const app = new Vue({
// //     vuetify,
// //     render: h => h(App),
// //     el: '#app',
// // });
