<?php

namespace Tests\Unit\Calculator\Operations;

use App\Calculator\Operations\Subtraction;
use PHPUnit\Framework\TestCase;

/**
 * Test subtraction operation
 */
class SubtractionTest extends TestCase
{
    /**
     * Test valid data
     *
     * @param array  $number
     * @param string $result
     *
     * @dataProvider validCaseProvider
     * @return void
     */
    public function testValid(array $number, string $result)
    {
        $this->assertEquals(
            (new Subtraction)->calc($number),
            $result
        );
    }

    /**
     * Test invalid data
     *
     * @param array  $number
     * @param string $result
     *
     * @dataProvider invalidCaseProvider
     * @return void
     */
    public function testInvalid(array $number, string $result)
    {
        $this->assertNotEquals(
            (new Subtraction)->calc($number),
            $result
        );
    }

    /**
     * Valid case provider
     *
     * @return array
     */
    public function validCaseProvider()
    {
        return [
            [
                ['12312315346456456546', '432432414522456545544'],
                '-420120099176000088998',
            ],
            [
                ['432424242423423423423432423', '78787687686876867876877678'],
                '353636554736546555546554745',
            ],
            [
                ['343243242545364576575675', '54654567356676766576576576'],
                '-54311324114131402000000901',
            ],
        ];
    }

    /**
     * Valid case provider
     *
     * @return array
     */
    public function invalidCaseProvider()
    {
        return [
            [
                ['434234234235453454343', '767575676575454545456'],
                '756756765756756756'
            ],
            [
                ['35345345345435345', '76765756756756765765756756'],
                '5645654547658769863454534'
            ],
            [
                ['656456456456456456', '765765756756756756756765'],
                '45645343534776557676767'
            ],
        ];
    }
}
