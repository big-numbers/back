<?php

namespace Tests\Unit\Calculator\Operations;

use App\Calculator\Operations\Addition;
use PHPUnit\Framework\TestCase;

/**
 * Test addition operation
 */
class AdditionTest extends TestCase
{
    /**
     * Test valid data
     *
     * @param array  $number
     * @param string $result
     *
     * @dataProvider validCaseProvider
     * @return void
     */
    public function testValid(array $number, string $result): void
    {
        $this->assertEquals(
            (new Addition)->calc($number),
            $result
        );
    }

    /**
     * Test invalid data
     *
     * @param array  $number
     * @param string $result
     *
     * @dataProvider invalidCaseProvider
     * @return void
     */
    public function testInvalid(array $number, string $result): void
    {
        $this->assertNotEquals(
            (new Addition)->calc($number),
            $result
        );
    }

    /**
     * Valid case provider
     *
     * @return array
     */
    public function validCaseProvider(): array
    {
        return [
            [
                ['12312315346456456546', '432432414522456545544'],
                '444744729868913002090',
            ],
            [
                ['545435435353124646346', '765675685842543454354343'],
                '766221121277896579000689',
            ],
            [
                ['343243242545364576575675', '54654567356676766576576576'],
                '54997810599222131153152251',
            ],
        ];
    }

    /**
     * Valid case provider
     *
     * @return array
     */
    public function invalidCaseProvider(): array
    {
        return [
            [
                ['434234234235453454343', '767575676575454545456'],
                '434545435435454543553'
            ],
            [
                ['89879797897845345349', '87676575676575654756'],
                '54354354353454354354'
            ],
            [
                ['56454353454354354354', '89872342343254454686'],
                '545'
            ],
        ];
    }
}
