<?php

namespace Tests\Unit\Calculator;

use App\Calculator\OperationNotFoundException;
use PHPUnit\Framework\TestCase;
use App\Calculator\OperationBuilder;
use App\Calculator\Operations;
use App\Models\Operation;

/**
 * Testing operation builder
 */
class OperationBuilderTest extends TestCase
{
    /**
     * Test addition operation create
     *
     * @return void
     * @throws OperationNotFoundException
     */
    public function testCreateAdditionOperation(): void
    {
        $this->assertInstanceOf(
            Operations\Addition::class,
            OperationBuilder::create(Operation\Id::ADDITION)
        );
    }

    /**
     * Test subtraction operation create
     *
     * @return void
     * @throws OperationNotFoundException
     */
    public function testCreateSubtractionOperation(): void
    {
        $this->assertInstanceOf(
            Operations\Subtraction::class,
            OperationBuilder::create(Operation\Id::SUBTRACTION)
        );
    }

    /**
     * Test try create undefined operation
     *
     * @return void
     * @throws OperationNotFoundException
     */
    public function testTryCreateUndefinedOperation(): void
    {
        $this->expectException(OperationNotFoundException::class);

        OperationBuilder::create(-1);
    }
}
