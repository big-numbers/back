<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('/', 'Api\AuthController@login');
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('refresh', 'Api\AuthController@refresh');
});

Route::group([
    'middleware' => ['api', 'auth'],
], function () {
    Route::post('operations/{id}/enable', 'Api\OperationController@enable');
    Route::post('operations/{id}/disable', 'Api\OperationController@disable');
});

Route::group([
    'middleware' => 'api',
], function () {
    Route::post('calculate', 'Api\CalculateController@calc');
    Route::resource('operations', 'Api\OperationController')
        ->only('index');
});
