# Quick start back    
    # Copy .env file
    cp .env.dev .env
    
    # Start docker containers from docker repository - https://gitlab.com/big-numbers/docker
    # Go into docker conteiner workspace
    cd ../laradock && docker-compose exec workspace bash 
    
    # Run command for init project  
    art fill:operations
    art fill:users

    # Set permissions 
    sudo chmod -R 777 ./storage
    sudo chmod -R 777 ./bootstrap/cache/
